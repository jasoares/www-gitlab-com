---
layout: markdown_page
title: "Sales Onboarding"
---

Every salesperson who starts at GitLab will enter an intense 30-day sales bootcamp.  The weekly class schedule can be found at [GitLab University](https://university.gitlab.com/).  In addition, once a new salesperson starts, their manager will create an issue for each of the first 4 weeks, tracking the progress of the new hire.

```
### Stage 1

**Familiarize yourself with the basics**

* [ ] Understanding [terminology](/glossary/).
   Please create a merge request for any term that does not have an answer or
   to add a term that you feel is relevant but is not on this page.

* [ ] Understanding Version Control Systems
    - [Presentation](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.g72f2e4906_2_29)

* [ ] Operating Systems and How Git Works
    - [Operating Systems and How Git Works](https://drive.google.com/a/gitlab.com/file/d/0B41DBToSSIG_OVYxVFJDOGI3Vzg/view?usp=sharing)
      recorded date: 2015-10-01

* [ ] Introduction to Git
    - [Intro to Git](https://www.codeschool.com/account/courses/try-git)
    - Supporting: [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html)

* [ ] [GitLab Workshop Part 1: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-1/material/)

* [ ] [GitLab Workshop Part 2: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-23370/material/)

* [ ] [GitLab Workshop Part 3: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-3/material/)

* [ ] [GitLab Workshop Part 4: Basics of Git and GitLab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-4/material/)

* [ ] [Making GitLab Great for Everyone, our response to the Dear GitHub letter](https://www.youtube.com/watch?v=GGC40y4vMx0)

* [ ] Create a GitLab Account
    - [Create a GitLab Account](https://courses.platzi.com/classes/git-gitlab/concepto/first-steps/create-an-account-on-gitlab/material/)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

* [ ] Add SSH key to GitLab
    - [Create and Add your SSH key to GitLab](https://www.youtube.com/watch?v=54mxyLo3Mqk)

* [ ] Repositories, Projects and Groups
    - [Recording](https://www.youtube.com/watch?v=4TWfh1aKHHw&index=1&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
      recorded date: 2015-12-10

* [ ] Creating a Project in GitLab
    - [Recording](https://www.youtube.com/watch?v=7p0hrpNaJ14)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

* [ ] Issues and Merge Requests
    - [Recording](https://www.youtube.com/watch?v=raXvuwet78M)
    - Supporting: [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

**Stage 1 Tests**

* [ ] [Version Control Systems Test](http://goo.gl/forms/8H8SNcH70T)
* [ ] [Intro to Git Test](http://goo.gl/forms/GgWF1T5Ceg)
* [ ] Create a group on GitLab.com named: "glu_yourname"
* [ ] Add your direct manager and Chad Malchow as master to the group
* [ ] Create a project "About Me" with a README.md with a list of things that we should know about you
* [ ] Create a project "90 day plan" with a README.md with what you plan to do in your first 90 days
* [ ] Create a MR adding something funny to either of the projects and assign to your manager and Chad Malchow to it
* [ ] Create an issue named: "Add me to TrainTool" and assign to Chad Malchow and tag your direct manager

### Stage 2

**Experience first hand the power of GitLab**

* [ ] Ecosystem
    - [Recording 2015-11-05](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
    - [GitLab Ecosystem slides](https://docs.google.com/presentation/d/1vCU-NbZWz8NTNK8Vu3y4zGMAHb5DpC8PE5mHtw1PWfI/edit)

* [ ] [Compare GitLab versions](/features/#compare)

* [ ] [GitLab compared to other tools](/comparison/)
    - [Compared to Atlassian (Recording 2016-03-03) ](https://youtu.be/Nbzp1t45ERo)

* [ ] [Positioning FAQ](https://about.gitlab.com/handbook/positioning-faq)

* [ ] Understanding DevOps?
    - [Understanding DevOps](https://youtu.be/HpZBnc07q9o)
    - [DevOps at IBM](https://www.youtube.com/user/IBMRational)
    - [DevOps Where To Start](https://www.youtube.com/watch?v=CSrKwP1QrjE)
    - [Agile & DevOps](https://www.youtube.com/watch?v=WqoVeGFjK9k)
    - [Problem Solving with DevOps](https://www.youtube.com/watch?v=pTq9hFBWPeM)

* [ ] JIRA and Jenkins integrations in GitLab
    - [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415).
      Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video

* [ ] GitLab Flow
    - [Recording of what it is](https://www.youtube.com/watch?v=UGotqAUACZA)
    - [GitLab Flow blog post](/2014/09/29/gitlab-flow/)
    - [GitLab Flow documentation](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)

* [ ] GitLab Integrations
    - Supporting: [Documentation on Integrating Jira with GitLab](http://doc.gitlab.com/ee/integration/jira.html) and [Demo of Jira integration within GitLab](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=44b548147a67ab4d8a62274047146415).  Download [WebEx](https://www.webex.com/play-webex-recording.html) to view this video
    - Supporting: [Atlassian Crowd feature request](http://feedback.gitlab.com/forums/176466-general/suggestions/4324384-integration-with-crowd)
    - Supporting: [Documentation on Integrating Jenkins with GitLab](http://doc.gitlab.com/ee/integration/jenkins.html)
    - Supporting: [Documentation on Integrating Bamboo with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/project_services/bamboo.md)
    - Supporting: [Documentation on Integrating Slack with GitLab](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/integration/slack.md)

* [ ] Migrating to GitLab from SVN/GitHub Enterprise/BitBucket/Perforce
    - Supporting: [Migrating from BitBucket/Stash](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_bitbucket.html)
    - Supporting: [Migrating from GitHub](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_github.html)
    - Supporting: [Migrating from SVN](http://doc.gitlab.com/ee/workflow/importing/migrating_from_svn.html)
    - Supporting: [Migrating from Fogbugz](http://doc.gitlab.com/ee/workflow/importing/import_projects_from_fogbugz.html)


**Stage 2 Tests**

* [ ] [Ecosystem Test](http://goo.gl/forms/5Vrf3CE0iC)
* [ ] Complete TrainTool Course - GitLab vs. The Competition
* [ ] Complete TrainTool Course - Jira Support
* [ ] Complete TrainTool Course - Selling EE to CE User
* [ ] Complete TrainTool Course - On-Premised vs. SaaS

### Stage 3

**Get familiar with GitLab's advanced features**

* [ ] [Compare GitLab versions](https://about.gitlab.com/features/#compare)

* [ ] Scalability and High Availability
    - [Recording 2015-12-03](https://www.youtube.com/watch?v=cXRMJJb6sp4&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=2)
    - [High Availability Documentation](https://about.gitlab.com/high-availability/)

* [ ] Managing LDAP, Active Directory
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=HPMjM-14qa8)

* [ ] Managing Permissions within EE
    - [Recording of what it is and how to set it up](https://www.youtube.com/watch?v=DjUoIrkiNuM)

* [ ] GitLab 8.2
    - [Recording 2015-11-19](https://www.youtube.com/watch?v=09RLHyMFfpA&index=3&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e)
    - [Slides](https://gitlab.com/gitlab-org/University/blob/master/classes/8.2.markdown) 2015-11-19

* [ ] Upcoming in EE and Big files in Git (Git LFS, Annex)
    - [Upcoming in EE](https://gitlab.com/gitlab-org/University/blob/master/classes/upcoming_in_ee.md)
    - [Big files in Git (Git LFS, Annex)](https://gitlab.com/gitlab-org/University/blob/master/classes/git_lfs_and_annex.md)

* [ ] [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)

* [ ] [Client Demo of GitLab with Job and Haydn](https://gitlabmeetings.webex.com/gitlabmeetings/ldr.php?RCID=ae7b72c61347030e8aa75328ed4b8660)

* [ ] [Sales Onboarding materials](/handbook/sales-onboarding/)

**Stage 3 tests**

* [ ] [GitLab 8.2 Test](http://goo.gl/forms/9PnmhiNzEa)
* [ ] [Big Files in Git Test](http://goo.gl/forms/RFsNK9fKuj)
* [ ] Complete TrainTool Course - Asking Effective Questions
* [ ] Complete TrainTool Course - Closing Techniques
* [ ] Complete TrainTool Course - Handling Pricing Objections
* [ ] Record a demo using WebEx and send to your manager and Chad Malchow.  Demo scenario is an organization with 500 seats who uses Jira and BitBucket for a few teams, subversion within a couple teams and GitLab CE is also installed.

### Stage 4 <a name="stage-4"></a>

* [ ] Login to [Salesforce.com](http://www.salesforce.com/), you should receive an email asking you to change your password.  Once you are in Salesforce, please familiarize yourself wth these reports/views as they will be critical in helping you manage your business.
* [ ] Review in order, learning material on our [Sales Process](https://about.gitlab.com/handbook/sales-process/) page.
* [ ] Review [Your Current Month Pipeline](https://na34.salesforce.com/00O61000001uYbM) This view is to focus you on what you are committing to closing this month.  Are you in the right stage? What is needed to advance the sale to the next stage?
* [ ] Review [Your Total Pipeline](https://na34.salesforce.com/00O61000001uYbR) This view should be used to identify where you are going and where you need focus to ensure you are successful.  What needs to to close and/or where you need to build up your pipleine - new business, expansion, add-on
* [ ] [Your Leads](https://na34.salesforce.com/00Q?fcf=00B610000027qT9&rolodexIndex=-1&page=1) This view should be used to make sure you are followign up on each lead in a timely manner and have a plan on how you qualify or disqualify a lead.
* [ ] Review [Your Personal Dashboard](https://na34.salesforce.com/01Z61000000J0gx) This dashboard should be used to understand where you have been, where you are at, where are you going and do you have the pipeline to get to where you need to be.
* [ ] [Accounts you Own](https://na34.salesforce.com/001?fcf=00B61000001XPLz) This view is to be used to identify expansion opportunities, who you have neglected and the mix of customers to prospects you are working


**Stage 4 test**
* [ ] Create Fake Lead in Salesforce.  Lead First Name - Test; Lead Last Name - GitLab Bootcamp.  Company: GitLab Sales Bootcamp. Lead Source: Other
* [ ] Convert Lead into an opportunity and Account
* [ ] Ensure Opportunity follows naming convention process
* [ ] Add Opportunity Product to Opportunity.  Quantity 2 of 100 seat Standard, one year.
* [ ] Change close date to be 90 days out
* [ ] Opportunity Stage to presenting solution

```


#### Supporting Information

1. [Getting Technical Support](https://about.gitlab.com/handbook/support/#internal)

1. [Our Sales Process](https://about.gitlab.com/handbook/sales-process/)

1. [Sales Best Practices Training](https://about.gitlab.com/handbook/sales-training/)

1. [Our Sales Agenda](https://docs.google.com/document/d/1l1ecVjKAJY67Zk28CYFiepHAFzvMNu9yDUYVSQmlTmU/edit)

1. [Recorded Practice Sales Demos](https://drive.google.com/drive/u/0/folders/0B_XVovPbWgADM1M3VUg1ZVJ0UjQ)
